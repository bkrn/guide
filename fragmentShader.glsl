precision mediump float;

// Passed in from the vertex shader.
varying vec4 vVertexColor;

void main() {
    gl_FragColor = vVertexColor;
}