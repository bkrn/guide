
#![feature(proc_macro, wasm_custom_section, wasm_import_module)]

extern crate wbg_rand;
extern crate noise;
extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;
use noise::{RidgedMulti, Seedable};
use noise::utils::{PlaneMapBuilder, NoiseMapBuilder};
use wbg_rand::{Rng, math_random_rng};

#[inline]
pub fn clamp<T: PartialOrd>(val: T, min: T, max: T) -> T {
    assert!(max >= min);
    match () {
        _ if val < min => min,
        _ if val > max => max,
        _ => val,
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}


#[wasm_bindgen]
extern {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet(name: &str) {
    alert(&format!("Hello, {}!", name));
}

const MAP_SIZE: usize = 250;
const SCALE: f32 = 0.125;

#[wasm_bindgen]
pub fn map_size() -> u16 {
    MAP_SIZE as u16
}

#[wasm_bindgen]
pub struct GuideMap {
    topo: Vec<f32>,
    pub max_height: f32,
    pub min_height: f32,
    pub length: u32,
    iso_indexes:  Vec<u32>,
}

#[wasm_bindgen]
impl GuideMap {

    pub fn get_topo(&self) -> Vec<f32> {
        return self.topo.clone()
    }

    fn generate_points(size: u32, relief: f32, contours: f32) -> (Vec<f32>, f32, f32) {
        let seed: u32 = math_random_rng().gen();
        let ns = RidgedMulti::new().set_seed(seed);
        let mp = PlaneMapBuilder::new(&ns)
            .set_size(size as usize, size as usize)
            .build();

        let to_scale = |v: usize| {
            let fsize = (size - 1) as f32;
            let scaled = (v as f32) / fsize * 2f32 - 1f32;
            scaled * 0.7
        };

        let mut topo = Vec::new();

        let insert = |x: usize, y: usize, z: f32, mp: &mut Vec<f32>| {
            mp.push(to_scale(x));
            mp.push(to_scale(y));
            mp.push(z);
        };

        let mut max: f32 = -2.0;
        let mut min: f32 = 2.0;


        let mut x: usize = 0;
        let mut y: usize = 0;

        let to_contour = |v: f64| {
            (((v + 1.0) * 0.5) as f32 * contours) as u32
        };

        while y < size as usize {
            let vals = [
                mp.get_value(x, y),
                mp.get_value(x+1, y),
                mp.get_value(x, y+1),
                mp.get_value(x+1, y+1),
            ];
            if to_contour(vals.iter().cloned().fold(2.0, f64::min)) != to_contour(vals.iter().cloned().fold(-2.0, f64::max)) {
                let sum: f64 = vals.iter().sum();
                let val: f32 =( sum as f32 / 4.0f32) * relief;
                if val < min {
                    min = val;
                }
                if val > max {
                    max = val;
                }
                insert(x,y,val,&mut topo);
            }
            x += 2;
            if x >= size as usize {
                x = 0;
                y += 2;
            }
        }
        (topo, min as f32, max as f32)
    }

    pub fn new(size: u32, relief: f32, contours: f32) -> GuideMap {
        let (topo, min, max) = GuideMap::generate_points(size, relief, contours);
        let len = topo.len();
        GuideMap{
            topo: topo,
            max_height: max,
            min_height: min,
            length: len as u32,
            iso_indexes: Vec::new(),
        }
    } 

}

#[wasm_bindgen]
pub fn draw_map() -> Vec<f32> {
    let seed: u32 = math_random_rng().gen();
    let ns = RidgedMulti::new().set_seed(seed);
    let mp = PlaneMapBuilder::new(&ns)
        .set_size(MAP_SIZE, MAP_SIZE)
        .build();

    let to_scale = |v: usize| {
        let fsize = (MAP_SIZE - 1) as f32;
        let scaled = (v as f32) / fsize * 2f32 - 1f32;
        scaled * 0.7
    };

    let mut map = Vec::with_capacity(3 * MAP_SIZE * MAP_SIZE);

    let insert = |x: usize, y: usize, z: f64, mp: &mut Vec<f32>| {
        mp.push(to_scale(x));
        mp.push(to_scale(y));
        mp.push((z as f32) * SCALE);
    };

    for x in 0..MAP_SIZE {
        for y in 0..MAP_SIZE {
            insert(x,y,mp.get_value(x, y),&mut map);
        }
    }
    map
}