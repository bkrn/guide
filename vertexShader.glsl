    attribute vec4 aVertexPosition;
    attribute vec4 aVertexColor;
    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;
    varying vec4 vVertexColor;
    
    void main() {
      gl_PointSize = 1.5;
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
      vVertexColor = aVertexColor;
    }